console.log("Hello World")

function addNum(num1, num2){
	return num1 + num2;
};

let sum = addNum(5, 15);
console.log("Displayed sum of 5 and 15");
console.log(sum)

function subNum(num3, num4) {
	return num3 - num4;
	
};

let difference = subNum(20, 5);
console.log("Displayed difference of 20 and 5:");
console.log(difference)

function multiplyNum(num5, num6) {
	return num5 * num6;
	
};

let product = multiplyNum(50, 10);
console.log("The product of 50 and 10:");
console.log(product)

function divideNum(num7, num8) {
	return num7 / num8;
	
};

let quotient = divideNum(50, 10);
console.log("The quotient of 50 and 10:");
console.log(quotient)

function getCircleArea(num9) {
	return 3.1416 * (num9 ** 2);
	
};

let circleArea = getCircleArea(15);
console.log("The result of getting the area of a circle with 15 radius.");
console.log(circleArea)

function getAverage(num10, num11, num12, num13) {
	return (num10 + num11 + num12 + num13) / 4

};

let averageVar = getAverage(20, 40, 60, 80);
console.log("The average of 20, 40, 60, 80");
console.log(averageVar)

function checkIfPassed(num14, num15) {
	return (num14 / num15) * 100
	
};

let isPassingScore = checkIfPassed(38, 50);
let isPassed = isPassingScore >= 75;
console.log("Is 38/50 a passing score?");
console.log(isPassed)
