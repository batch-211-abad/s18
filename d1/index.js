console.log("Hello World");

//Functions

	//Parameters and Arguments

	function printInput() {
		let nickName = prompt("Enter your nickname: ");
		console.log("Hi, " + nickName);
	}

	printInput();

	function printName(name) {
		console.log("My name is " + name);
	};

	printName("Juana");

	//you can directly pass data into the function
	//the function can then call/use that data which is referred as "name" within the function
	//"name" is called a parameter
	//a "parameter" acts as a named variable/container that exists only inside of a function
	//it is used to store informationthat is provided to a function when it is called or invoked

	//"Juana", the information/data provided directly into a function called an argument.

	printName("John");
	printName("Jane");

	//variables can also be passed as an argument
	let sampleVariable = "Yui";
	printName(sampleVariable);

	//Function arguments cannot be used by a function if there are no parameters provided within the function.

	function checkDivisibilityBy8(num){
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?")
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);

	/*
		create a function that can check the divisibility by 4
		1. 56
		2. 95
	*/

	function checkDivisibilityBy4(num){
		let remainder1 = num % 4;
		console.log("The remainder of " + num + " divided by 4 is: " + remainder1);
		let isDivisibleBy4 = remainder1 === 0;
		console.log("Is " + num + " divisible by 4?")
		console.log(isDivisibleBy4);
	}

	checkDivisibilityBy4(56);
	checkDivisibilityBy4(95);

	//you can also do th same using prompt(), however, take note that prompt() outputs a string. Strings ae not ideal for mathematical computations

	//Functions as arguments
	//Function parameters can also accept other functions as arguments
	//Some complex functions use other functions as argumentsto perform more complicated results.
	//This will be further seen when we discuss array methods.

	function argumentFunction() {
		console.log("This function was passed as an argument before the message was printed.")
	};

	function invokeFunction(argumentFunction){
		argumentFunction();
	};

	//Adding and removing the parentheses "()" impacts the output of JavaScript heavily
	//when a function is used with parenthesis "()", iot denotes invoking/calling a function
	invokeFunction(argumentFunction);	

	console.log(argumentFunction)


	function createFullName(firstName, middleName, lastName){
		console.log(firstName + ' ' + middleName + ' ' + lastName);

	}

	createFullName("Juan", "dela", "Cruz");

	createFullName("Juan", "dela");
	createFullName("Juan", "dela", "Cruz", "Hello");

	let firstName = "John"
	let middleName = "Doe"
	let lastName = "Smith"

	createFullName(firstName, middleName, lastName)

	/* 
		create a function called printFriends
		3 parameters 
		My three friends are: friend1, friend2, friend3
	*/

	function printFriends(friend1, friend2, friend3){
		console.log("My three friends are " + friend1 + ", " + friend2 + ", " + friend3);

	}

	printFriends("Ash", "Misty", "Brock");


	function returnAddress(city, country){
		let fullAddress = city + ", " + country;
		return fullAddress
	};

	let myAddress = returnAddress("Cebu City", "Philippines");
	console.log(myAddress);



